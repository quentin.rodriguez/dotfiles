# Dotfiles

## Installation

1. To automate the installation of **dotfiles**, you can copy and paste the command below

``` sh
curl -s https://gitlab.com/quentin117/dotfiles/-/raw/main/install | bash
```

2. Follow the steps to install the **dotfiles** manually

``` sh
git clone https://gitlab.com/quentin117/dotfiles.git ~/.dotfiles
```